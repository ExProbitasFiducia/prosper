import { makeExecutableSchem, addResolversToSchema } from "graphql-tools/schema";

const graphqlResolver = new Map();

const graphqlResolver = (target, propertyKey, descriptor) => {
    graphqlResolverMap.set(target, {
        ...graphqlResolverMap.get(target),
        [propertyKey]: descriptor.value
    });
}



const typeDefs = `
    ${graphqlTypeDefs.join("")}
`

export const schema = makeExecutableSchema({ typeDefs });

graphqlResolverMap.entries().forEach(([target, resolvers]) => {
    addResolversToSchema({ schema, resolvers, updateResolversInPlace: true });
});
