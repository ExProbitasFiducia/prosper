import { build } from "npm:esbuild";
import { denoPlugin } from "https://deno.land/x/esbuild_deno_loader@0.5.2/mod.ts";
import { importMapPlugin } from "./plugins/import-map.ts";

export const buildWeb = () => await build({
    entryPoints: ["./src/index.tsx"],
    loader: {
        ".mjs": "js",
        ".ts": "ts",
        ".tsx": "tsx",
        ".css": "css"
    },
    plugins: [
        importMapPlugin
    ],
    write: false,
    bundle: true,
    format: "esm",
    absWorkingDir: Deno.cwd(),
    external: [
        "react",
        "react-dom"
    ]
});
