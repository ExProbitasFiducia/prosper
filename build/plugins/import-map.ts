
export const importMap: Record<string, string> = {
    "react": "https://esm.sh/react@18",
    "react-dom": "https://esm.sh/react-dom@18"
};

export const importMapPlugin = {
    name: "import-map",
    setup: (build) => {
        build.onResolve(
            { filter: /.*?/ },
            (args: any) => {
                const mappedPath = importMap[args.path];
                if (
                    (mappedPath ?? "").startsWith("http") ||
                    (args.path ?? "").startsWith("http")
                ) {
                    return {
                        path: mappedPath ?? args.path,
                        external: true
                    };
                }
                return {};
            }
        );
    }
};

