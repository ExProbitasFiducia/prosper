import {
    staticDirs,
    createStaticDirServer
} from "./handlers/static.ts";
import { serveSSR } from "/handlers/ssr.ts";

import { pathToRegexp } from "https://raw.githubusercontent.com/pillarjs/path-to-regexp/master/src/index.ts";

export const routes = [
    ...staticDirs.map(staticDir => {
        return ["/(.*)", createStaticDirServer(staticDir)];
    }),
    ["/(.*)", () => serveSSR(request, pathname)]
].map(([ path, handler ]) => [ pathToRegexp(path), handler ];
