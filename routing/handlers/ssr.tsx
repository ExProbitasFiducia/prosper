import { RequestProps } from "./types.d.ts";



export const serveSSR = async ({
    request: request,
    pathname: string
}: RequestProps): Promise<Response> => {
    const gqlClient = new GraphQLClient({
        link: new SchemaLink({ schema })
    });
    const sheet = new ServerStyleSheet() as any;
    const renderToString(sheet.collectStyles(
        <GraphQLProvider
            client={gqlClient}
        >
            <StaticRouter>
                <App />
            </StaticRouter>
        </GraphQLProvider>
    ));

    const indexPage = indexTemplate(
        rendered as string,
        sheet.getStyleTags as string
    );

    return new Response({
        status: 200,
        headers: {
            "content-type": "text/html"
        }
    });
}

