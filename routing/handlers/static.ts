
export const staticDirs = [
    "./public",
    "./dist"
];

export const resolvePath = (pathname) => posix.join(staticDir, posix.normalize(decodeURI(pathname)));

export const serveStaticFile = async ({}: RouteProps): Promise<Response> => {
   return new Response(`<doctype html>
   <html>${indexJs}

`, { 
       status: 200,
       headers: {
           "content-type": "text/html"
       } 
   }); 
};

export const createStaticDirServer = (staticDir) => async ({
    request, pathname
}: RouteProps) => {
    const path = resolvePath(pathname);
    const { isDirectory, isFile } = await Deno.stat(path);
    if (isDirectory) {
        // return await serveDir(request, path);
        return;
    }
    if (isFile) {
        return await serveFile(request, path);
    }
}
