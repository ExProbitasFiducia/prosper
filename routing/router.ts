import { pathToRegexp } from "https://raw.githubusercontent.com/pillarjs/path-to-regexp/master/src/index.ts";
import { createRouterHandler } from "./utils.ts";

import { serveSSR } from "./handlers/ssr.tsx";
import { serveStaticFile } from "./handlers/static.ts";

const routes = [
    ["/(.*)", createStaticDirServer("/dist")],
    ["/(.*)", serveSSR]
].map(([ path, handler ]) => [ pathToRegexp(path), handler ]);
 
export const router = async (request, connInfo): Promise<Response> => {
    const { pathname } = new URL(request.url);
    return await routes
        .map(createRouteHandler({ request, pathname }))
        .find(promiseFulfills);
};
