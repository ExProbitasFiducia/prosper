export const createRouteHandler = ({ request, pathname }) => {
    return async ([ regexp, handler ]) => {
        console.log(regexp)
        const match = regexp.exec(pathname);
        if (!match) {
            throw "NO MATCH";
        }
        return await handler({
            request,
            pathname,
            match
        });
    };
};
