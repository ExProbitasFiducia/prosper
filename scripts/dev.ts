const buildProcess = Deno.run({
    cmd: ["deno", "run", "../src/web/build.ts"]
});

await buildProcess.status;

const serverProcess = Deno.run({
    cmd: ["deno", "run", "../src/server.ts"]
});

