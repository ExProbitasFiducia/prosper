import { serve } from "https://deno.land/std/http/server.ts";

import { promiseFulfills } from "./utils.ts";

import { router } from "./router.ts"; 

const serverOptions = {
    port: 9000
};

export const startServer = () => serve(router, serverOptions);

if (import.meta.main) {
    await startServer();
}
