export const spellbook = {
    help: () => {
        console.log("I'm sorry, dave. Im afraid I cant do that.");
    },
    dev: () => {
        // run the development environment
        console.log("starting local dev environment");
    },
    build: () => {
        // transpile all the frontend assets and put them in the /dist dir.
        console.log("building the static assets");
    }
};
