export const promiseFulfills = async (p) => (await p) !== undefined;
