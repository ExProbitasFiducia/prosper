import React from "react";

// Mocked data for the high-priority tasks
const highPriorityTasks = [
  { id: 1, title: "Finish report for the meeting" },
  { id: 2, title: "Prepare presentation for the client" },
  { id: 3, title: "Follow up on project milestones" },
  { id: 4, title: "Submit expense report" },
];

// HighPriorityTasks component that shows the list of high-priority tasks
const HighPriorityTasks: React.FC = () => {
  return (
    <ul>
      {highPriorityTasks.map((task) => (
        <li key={task.id}>{task.title}</li>
      ))}
    </ul>
  );
};

// Root component that renders the HighPriorityTasks component
const App: React.FC = () => {
  return (
    <div>
      <h1>High-Priority Tasks</h1>
      <HighPriorityTasks />
    </div>
  );
};

export default App;
