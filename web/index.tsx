import React from "react";
import { hydrate } from "react-dom";

import App from "./App.tsx";

// Hydrate the React app with the existing HTML content
ReactDOM.hydrate(<App />, document.getElementById("root"));
